/*   
Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 1.9.0
Author: Sean Ngu
Website: http://www.seantheme.com/color-admin-v1.9/admin/
*/
function date2string(date) {
    var yyyy = date.getFullYear();
    var mm = (date.getMonth() + 1).toString();
    var dd  = date.getDate().toString();
    return (dd[1] ? dd : '0' + dd[0]) + '/' +
           (mm[1] ? mm : '0' + mm[0]) + '/' + yyyy;

}

var handleCalendarDemo = function() {
    "use strict";
    var e = {
        left: "today prev,next ",
        center: "title",
        right: "month,agendaWeek,agendaDay"
    };
    var t = new Date;
    var n = t.getMonth();
    var r = t.getFullYear();
    var i = $("#calendar").fullCalendar({
        defaultView: 'agendaWeek',
       timeFormat: {
        agenda: 'h(:mm)t{ - h(:mm)t}',
        '': 'h(:mm)t{-h(:mm)t }'
       },
       monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ], 
       monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
       dayNames: [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
       dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
       buttonText: {
        today: 'hoy',
        month: 'mes',
        week: 'semana',
        day: 'día'
       },
        header: e,
        selectable: true,
        selectHelper: true,
        droppable: true,
        drop: function(e, t) {
            var nn = $(this).data("eventObject");
            $('[name=fechaactual]').val(date2string(arguments[0]));
            $('[name=descripcion]').val(nn['description']);
            var alert = $('#modal-calendar');
            alert.modal('show');
            alert.find('.modal-title').html('Registrar evento en calendario: ' + nn['title']);
            alert.find('.btn-default').click(function(){
                var r = $.extend({}, nn);
                r.start = e;
                r.allDay = t;
                $("#calendar").fullCalendar("renderEvent", r, true);
                alert.modal('hide');
            });
        },
        select: function(e, t, n) {
            var r = prompt("Event Title:");
            if (r) {
                i.fullCalendar("renderEvent", {
                    title: r,
                    start: e,
                    end: t,
                    allDay: n
                }, true)
            }
            i.fullCalendar("unselect")
        },
        eventRender: function(e, t, n) {
            var r = e.media ? e.media : "";
            var i = e.description ? e.description : "";
            t.find(".fc-event-title").after($('<span class="fc-event-icons"></span>').html(r));
            t.find(".fc-event-title").append("<small>" + i + "</small>")
        },
        eventClick: function(e, t, v) {
            // var alert = $('#modal-calendar');
            // $('[name=fechaactual]').val(date2string(e.start));
            // $('[name=descripcion]').val(e.description);
            // alert.find('.modal-title').html('Actualizar evento en calendario: ' + e.title);
            // alert.modal('show');
            var alert = $('#modal-danger');
            alert.modal('show');
        },
        editable: true
    });
    $("#external-events .external-event").each(function() {
        var e = {
            title: $.trim($(this).attr("data-title")),
            className: $(this).attr("data-bg"),
            media:  "<span class='label label-info'><i class='fa fa-edit'></i></span><span class='label label-danger'><i class='fa fa-remove'></i></span>",
            description: $(this).attr("data-desc")
        };
        $(this).data("eventObject", e);
        $(this).draggable({
            zIndex: 999,
            revert: true,
            revertDuration: 0
        })
    });
}
;
var Calendar = function() {
    "use strict";
    return {
        init: function() {
            handleCalendarDemo()
        }
    }
}()
